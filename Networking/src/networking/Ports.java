/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package networking;

/**
 *
 * @author fx62 & Luxo Lederhand
 */
public class Ports {
    
    private int source;
    private int destination;
    private boolean connected;
    
    public Ports(int source, int destination){
        this.source = source;
        this.destination = destination;
        this.connected = false;
    }
    
    public int getSource(){
        return source;
    }
    
    public int getDestination(){
        return destination;
    }
    
    public boolean getConnected(){
        return connected;
    }
    
    public void setSource(int source){
        this.source = source;
    }
    
    public void setDestination(int destination){
        this.destination = destination;
    }
    
    public void setConnected(boolean connected){
        this.connected = connected;
    }
}
