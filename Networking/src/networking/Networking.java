/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package networking;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Scanner;


/**
 *
 * @author fx62 & Luxo Lederhand
 */
public class Networking {

    
        private static final Scanner sc = new Scanner(System.in);
        // create Temerary Enterprise Moderm Production Inventory tempInventory
        private static final ArrayList<Devices> tempInventory = new ArrayList<Devices>();
        private static final ArrayList<String> messageLog = new ArrayList<String>(); 
    /**
     * @param args the command line arguments
     */
public static void main(String[] args) {
        //mainMenu();
        // true - en uso
        // false - disponible
        //ArrayList<String> arp = null;
        /*Ports[] port0 = new Ports[1];
        Ports[] port1 = new Ports[1];
        Ports[] port1a = new Ports[1];
        Ports[] port2 = new Ports[4];
        Ports[] port3 = new Ports[4];
        Ports[] port4 = new Ports[2];
        Ports[] port5 = new Ports[2];
        Hosts host1 = new Hosts("Host", "H1", arp, "1.1", port0, 1);
        Hosts host2 = new Hosts("Host", "H2", arp, "2.2", port1, 1);
        //Hosts host3 = new Hosts("Host", "H3", arp, "1.2", port1a, 1);
        Switches switch1 = new Switches("Switch", "S1", arp, "1.0", port2, 4);
        Switches switch2 = new Switches("Switch", "S2", arp, "2.0", port3, 4);
        Routers router1 = new Routers("Router", "R1", arp, "1.0", port4, 2);
        Routers router2 = new Routers("Router", "R2", arp, "2.0", port5, 2);
        router1.SetIP2("2.0");
        router2.SetIP2("3.0");
        host1.instancePorts(port0, 1);
        host2.instancePorts(port1, 1);
        switch1.instancePorts(port2, 4);
        switch2.instancePorts(port3, 4);
        router1.instancePorts(port4, 2);
        router2.instancePorts(port5, 2);
        tempInventory.add(host1);
        tempInventory.add(switch1);
        tempInventory.add(router1);
        tempInventory.add(switch2);
        tempInventory.add(host2);
        //tempInventory.add(router2);
        //struct();
        //showDevices();
        */
        mainMenu();
    }
    
    // this menu only calls other methods and some menus
    public static void mainMenu(){
        boolean control = true;
        String action = "";
        //this do do things
        do {
            System.out.println("");
            System.out.println("1 - Agregar dispositivo");
            System.out.println("2 - Mostrar  dispositivos");
            System.out.println("3 - Eliminar dispositivos");
            System.out.println("4 - Crear conexiones");
            System.out.println("5 - Mensajeria compartida");
            System.out.println("6 - Salir");
            action = sc.nextLine();  
            switch(action){
                case "1":
                    addMenu();
                    break;
                case "2":
                    showDevices();
                    break;
                case "3":
                    deletedDevices();
                    break;
                case "4":
                    struct();
                    break;
                case "5":
                   menuMessage();
                    break;
                case "6":
                    System.out.println("*****Gracias y vuelva pronto*****");
                    control = false;
                    break;
                default:
                    System.out.println("El valor ingresado no es valido");
                    break;
            }
        } while (control);
    }
    
    // this menu adds devices such like the user wants to do it
    public static void addMenu(){
        String action = "";
        System.out.println("1 - Agregar Router");
        System.out.println("2 - Agregar Switch");
        System.out.println("3 - Agregar Host");
        action = sc.nextLine();  
        switch(action){
            case "1":
                Router();
                break;
            case "2":
                Switch();
                break;
            case "3":
                Host();
                break;
            default:
                System.out.println("El valor ingresado no es valido");
        }
    }
        
    // verify if the ip must be an segment or an ip x.y
    // true if it's a segment or
    // false if it's an ip
    public static String addIP(boolean network){
        boolean control = true;
        short first = 0;
        short second = 0;
        int dot = -1;
        //String net = "";
        String ip = "";
        do {
            System.out.println("IP [X.Y]: ");
            try {
            ip = sc.nextLine();
            //try {
                dot = ip.indexOf('.');
            } catch (Exception e){
            }
            if (dot != -1){
            //System.out.println("*");
                try {
                    //System.out.println(ip.substring(0, dot));
                    //System.out.println(ip.substring(dot+1, ip.length()));
                    first = Short.parseShort(ip.substring(0, dot));
                    second = Short.parseShort(ip.substring(dot+1, ip.length()));
                    if (first > 0 && first < 255 && second < 255 && second >= 0){
                        if (network){
                            if(second == 0){
                                control = false;
                            } else {
                                System.out.println("A los routers y switches unicamente se "
                                        + "pueden asignar segmentos de red X.0\n\n");
                            }
                        } else {
                            if(second != 0){
                                if (duplicated(ip)){
                                    System.out.println("Esta ip ya ha sido asignada anteriormente\n");
                                }else {
                                    control = false;
                                }
                            } else {
                                System.out.print("No es posible asignar un "
                                        + "segmento de red a este dispositivo\n\n");
                            }
                        }
                    } else {
                        System.out.println("El valor ingresado no se encuentra "
                                + "en el rango de ips validas\n\n");
                    }
                } catch (Exception e){
                    System.out.println("La ip ingresada no es valida\n\n");
                }
            } else {
                System.out.println("La ip ingresada no es valida\n\n");
            }
            /*} catch (Exception e){
                System.err.println("La ip ingresada no es valida");
            }*/
        } while(control);
        return ip;
    }
    
    public static boolean duplicated(String ip){
        boolean control = false;
        for (int i = 0; i < tempInventory.size(); i++){
            if (tempInventory.get(i).getType().equals("Host")){
                if (ip.equals(tempInventory.get(i).getIP()))
                    control = true;
            }
        }
        return control;
    }
    
    public static int addPort() 
    {
       int nPort=0;
       String port;
       boolean control = true;
       do { 
        System.out.println("Ingrese el numero de puertos");   
        try{
        port = sc.nextLine();
        nPort = Integer.parseInt(port);
        if (nPort > 2 && nPort <= 48){
            control = false;
        } else if(nPort <= 2){
            System.out.println("No es posible asignar menos de 3 puertos a este dispositivo");
        } else {
            System.out.println("La cantidad maxima de puertos a utilizar es de 48");
        }
        }catch(Exception e){
            System.out.println("Ingrese un numero valido");
        }            
        } while (control);
        return nPort;
    }
    
    // make a valid name with more o equal to 1 character
    public static String addName(){
        boolean control = true;
        String name = "";
        do {
            try {
                System.out.println("Nombre: ");
                name = sc.nextLine();
                if (name.length() != 0){
                    control = false;
                } else{
                    System.out.println("No es posible dejar vacio este campo\n");
                }
            } catch (Exception e){
                System.err.println("El nombre del dispositivo no puede "
                        + "encontrarse vacio");
                control = false;
            }
        } while(control);
        return name;
    }
    
    // this instances the host devices
    public static void Host(){
        String name = addName();
        String ip = addIP(false);
        //ArrayList<String> arp = new ArrayList<String>();
        Ports[] ports = new Ports[1];
        Hosts hostDevice = new Hosts("Host", name, ip, ports, 1);
        hostDevice.instancePorts(ports, 1);
        tempInventory.add(hostDevice);
    }
    
    // this instances the switch devices
    public static void Switch(){
        String name = addName();
        //ArrayList<String> arp = new ArrayList<String>();
        System.out.println("NOTA: El segmento de red utilizado en el switch "
                + "debe de ser el mismo segmento "
                + "del router al que se encuentra conectado");
        String ip = addIP(true);
        int nports = addPort();
        Ports[] ports = new Ports[nports];
        Switches switchDevice = new Switches("Switch", name, ip, ports,
        nports);
        switchDevice.instancePorts(ports, nports);
        tempInventory.add(switchDevice);
    }
    
    // this instances the router devices
    public static void Router(){
        String name = addName();
        System.out.println("LAN");
        String ip = addIP(true);
        String ip2 = "Give me the powa";
        do {
            System.out.println("WAN");
            ip2 = addIP(true);// do while ip =! ip2      
            if(ip.equals(ip2))
            {
//                System.out.println("Impossible to create the conexion, The segment is alredy in use"
//                +" Please, contact to the technical support or call to 1-800-399-4585 for more information"
//                +" el consumo de este producto puede ser perjudicial para la saludo. Please search for help, you are not alone"
//                + " dont killyourself plox"); 
                System.out.println("ERROR #272\nImposible crear la conexion, el segemento indicado ya esta en uso"
                +" con el puerto lan. Intente nuevamente");
            }
           } while (ip.equals(ip2));
        //ArrayList<String> arp = new ArrayList<String>();
        Ports[] ports = new Ports[2];
        Routers routerDevice = new Routers("Router", name, ip, ports,
        2);
        routerDevice.SetIP2(ip2);
        routerDevice.instancePorts(ports, 2);
        //System.out.println(routerDevice.getIP2()); Print the Ip2
        tempInventory.add(routerDevice);
    }
    
    // this method checks if the input is an integer value and show the message that gets on the arguments
    public static int inputInt(String message, int max){
        boolean control = true;
        int n = 0;
        do{
            System.out.print(message);
            try {
                n = Integer.parseInt(sc.nextLine());
                if (n >= 0 && n < max){
                    control = false;
                } else if (n == -6426){
                    control = false;
                } else {
                    System.out.println("El valor ingresado se encuentra "
                            + "fuera del rango\n");
                }
            } catch (Exception e){
                System.out.println("El valor ingresado no es valido\n");
            }
        } while (control);
        return n;
    }
    
    // this method returns how much ports are available
    public static int portsAvailables(){
        int n = 0;
        for (int i = 0; i < tempInventory.size(); i++){
            Ports[] ports = tempInventory.get(i).getPorts();
            if(!isAvailable(tempInventory.get(i))){
                n++;
            }
        }
        return n;
    }    
    
    // check if there are ports available
    //*** I need to check a second purpose for this method***//
    public static boolean isAvailable(Devices device){
        boolean available = true;
        Ports[] ports = device.getPorts();
        for (int i = 0; i < ports.length; i++){
            if (!device.getActive()){
                if(!ports[i].getConnected()){
                    available = false;
                    break;
                }
            }
        }
        /*if (available){
            device.setActive(true);
        }*/
        return available;
    }
    
    public static void showPorts(Devices device){
        Ports[] ports = device.getPorts();
        for (int i = 0; i < device.getNPorts(); i++){
            if (ports[i].getConnected()){
                System.out.println("    Puerto " + i + " activo");
            } else {
                System.out.println("    Puerto " + i + " inactivo");
            }
        }
    }
    
    // ask number of port to the user
    public static int checkPort(Devices device, int max){
         int nports = device.getNPorts();
         int port = 0;
         boolean control = true;
         do {
             if (device.getType().equals("Switch")){
                 showPorts(device);
                port = inputInt("Ingrese el numero del puerto a conectar [0-" + (nports - 1) + "] ", max);
             } else if (device.getType().equals("Router")){
                port = inputInt("    0 - Puerto LAN\n    1 - Puerto WAN\n", 2);
             } else {
                port = 0;
             }
             Ports[] portsDevice = device.getPorts();
             if (portsDevice[port].getConnected()){
                 System.out.println("El puerto indicado ya se encuentra en uso");
             } else{
                 control = false;
             }
         } while(control);
         return port;
    }

    // give the ip of the device with the input port user
    public static String giveIP(Devices device, int port){
        String ip = "";
        if (device.getType().equals("Router")){
            if(port == 0){
                ip = device.getIP();
            } else {
                ip = device.getIP2();
            }
        } else {
            ip = device.getIP();
        }
        return ip;
    }
    
    // check if both ip are in the same segment
    public static boolean isEqual(String device1, String device2)
    {
       boolean trig = false;
        String[] partsDevice1 = device1.split("\\.");
        String[] partsDevice2 = device2.split("\\.");
        
        if(partsDevice1[0].equals(partsDevice2[0]))
        {
            trig = true;
        } else {
            System.out.println("Imposible crear conexion, verifique que los dispositvos"
             + " sean compatibles o que tengan puertos disponibles e intente de nuevo\n");
        }
        return trig;
    }
    
    // this fucking method need to be better
    public static void struct(){
        ArrayList<Integer> temp = new ArrayList<Integer>();
        int device1, device2, n1 = 0, n2 =0;
        String ip1 = "", ip2 = "";
        Devices d1 = null;
        Devices d2 = null;
        if(portsAvailables() >= 2){
            System.out.println("Dispositivos disponibles: ");
            for (int i = 0; i < tempInventory.size(); i++){
                d1 = tempInventory.get(i);
                Ports[] ports = d1.getPorts();
                if (!isAvailable(d1)){
                    if(d1.getType().equals("Router")){
                        System.out.print(i + " - " +tempInventory.get(i).getType()
                        + " " + tempInventory.get(i).getName() + ": ");
                        System.out.print(tempInventory.get(i).getPorts()[0].getConnected() ?
                                "Puerto en uso " : "Puerto disponible ");
                        System.out.print(tempInventory.get(i).getIP() + " | ");
                        System.out.print(tempInventory.get(i).getPorts()[1].getConnected() ? 
                                "Puerto en uso " : "Puerto disponible ");
                        System.out.println(tempInventory.get(i).getIP2());
                    } else{
                         System.out.println(i + " - " +tempInventory.get(i).getType()
                         + " " + tempInventory.get(i).getName() + ": " + 
                         tempInventory.get(i).getIP());
                     }
                } else {
                     temp.add(i);
                 }
            }
            boolean control = true;
            do {
                device1 = inputInt("Ingresar el numero del dispositivo a conectar: ", tempInventory.size());
                control = temp.contains(device1);
                if (control){
                    System.out.println("El dispositivo " + device1 + " ya se encuentra conectado a la red, no es posible"
                    + " utilizarlo\n");
                } else {
                    d1 = tempInventory.get(device1);
                    n1 = checkPort(d1, d1.getNPorts());
                    control = false;
                }
            } while (control);
            d1 = tempInventory.get(device1);
            ip1 = giveIP(d1, n1);
            System.out.println(ip1+ "\n");
            temp.clear();
            System.out.println("Dispositivos disponibles: ");//this print.... eam.. something, I guess
            for (int i = 0; i < tempInventory.size(); i++){
                d2 = tempInventory.get(i);
                Ports[] ports = d2.getPorts();
                if (!isAvailable(d2) && i != device1){
                    if(d2.getType().equals("Router")){
                        System.out.println(i + " - " +tempInventory.get(i).getType()
                        + " " + tempInventory.get(i).getName() + ": " + 
                        tempInventory.get(i).getIP() + " | " + tempInventory.get(i).getIP2());
                    } else{
                         System.out.println(i + " - " +tempInventory.get(i).getType()
                         + " " + tempInventory.get(i).getName() + ": " + 
                         tempInventory.get(i).getIP());
                     }
                } else {
                     temp.add(i);
                }
            }
            control = true;//this is a boolean
            do {
                d1 = tempInventory.get((device1));
                device2 = inputInt("Ingresar el numero del dispositivo a conectar: ", tempInventory.size());
                control = temp.contains(device2);
                if (device2 == device1){
                    System.out.println("No es posible hacer la conexion en un unico dispositivo\n");
                } else if (control){
                    System.out.println("El dispositivo " + device2 + " ya se encuentra conectado a la red, no es posible"
                    + " utilizarlo\n");
                } else {
                    d2 = tempInventory.get((device2));
                    n2 = checkPort(d2, d2.getNPorts());
                    ip2 = giveIP(tempInventory.get(device2), n2);
                    System.out.println(ip2+"\n");
                    control = true;
                    if(isEqual(ip1, ip2)) {
                        connect(d1, device1, device2, n1);
                        /*Ports[] ports = d1.getPorts();
                        System.out.println(ports[0].getConnected());
                        System.out.println(ports[0].getDestination());
                        System.out.println(ports[0].getSource());*/
                        connect(d2, device2, device1, n2);
                        /*ports = d2.getPorts();
                        /*System.out.println(ports[0].getConnected());
                        System.out.println(ports[0].getDestination());
                        System.out.println(ports[0].getSource());
                        mainMenu();*/
                        System.out.println("***Coneccion creada. Felicidades***");
                        control = false;
                    } else{
                        if(tempInventory.size() == 2){
                            control = false;
                        } else {
                            int retry = inputInt("    0 - Intentar realizar la conexion con otro dispositivo\n"
                                    + "    1 - Regresar al menu principal\n", 2); //Luis was here
                            if (retry == 1){
                                //mainMenu();
                                control = false;
                            }
                        }
                    }
                }
            } while (control);
            
            
        } else {
            System.out.println("Es necesario un minimo de 2 dispositivos para "
                    + "establecer una conexion de red\n\n");
        }
    }
    
    public static void connect(Devices device, int source, int destination, int port){
        Ports[] ports = device.getPorts();
        ports[port].setConnected(true);
        ports[port].setSource(source);
        ports[port].setDestination(destination);
        device.SetPorts(ports);
    }
    
    // this is the third option of the mainMenu/
    public static void showDevices(){
        int ct = 0;// ct for criterial
        if (tempInventory.size() > 0){
            for (int i = 0; i < tempInventory.size(); i++){
                Devices device = tempInventory.get(i);
                System.out.println(device.getType() + " " + device.getName());
                if(device.getType().equals("Router")){
                    if(device.getPorts()[0].getConnected()) {
                        System.out.println("    Puerto LAN activo, IP: " + device.getIP());
                    } else{
                        System.out.println("    Puerto LAN inactivo, IP: " + device.getIP());
                    }
                    if(device.getPorts()[1].getConnected()) {
                        System.out.println("    Puerto WAN activo, IP: " + device.getIP2());
                    } else{
                        System.out.println("    Puerto WAN inactivo, IP: " + device.getIP2());
                    }
                } else if (device.getType().equals("Switch")){
                    System.out.println("    " + "Segmento: " + device.getIP());
                    for (int j = 0; j < device.getPorts().length; j++){
                        if (device.getPorts()[j].getConnected()){
                            System.out.println("    Puerto " + j + ": Activo");
                        } else{
                            System.out.println("    Puerto " + j + ": Inactivo");
                        }
                    }   
                } else {
                    if (device.getPorts()[0].getConnected()) {
                        System.out.println("    " + "Puerto activo,  IP: " + device.getIP());
                    } else{
                        System.out.println("    " + "Puerto inactivo,  IP: " + device.getIP());
                    }
                }
                System.out.println();
            }
        } else{
            System.out.println("Es necesario al menos un dispositivo para utilizar esta opcion\n");
            // Hi, i´m a comment
        }
        
    }
    
    public static void deletedDevices(){//this method delete a device
        //ArrayList<Integer> temp = new ArrayList<Integer>();
        int device1; //, device2;
        Devices d1 = null;
        Devices d2 = null;// Hi, im useless
        if(tempInventory.size() > 0){
            System.out.println("Dispositivos disponibles: ");
            for (int i = 0; i < tempInventory.size(); i++){
                d1 = tempInventory.get(i);
                //Ports[] ports = d1.getPorts();
                    if(d1.getType().equals("Router")){
                        System.out.println(i + " - " +tempInventory.get(i).getType()
                        + " " + tempInventory.get(i).getName() + ": " + 
                        tempInventory.get(i).getIP() + " | " + tempInventory.get(i).getIP2());
                    } else{
                         System.out.println(i + " - " +tempInventory.get(i).getType()
                         + " " + tempInventory.get(i).getName() + ": " + 
                         tempInventory.get(i).getIP());
                     }
//                    temp.add(i);
               }
//            boolean control = true;
            device1 = inputInt("Ingresar el numero del dispositivo a eliminar: ", tempInventory.size());
                    d1 = tempInventory.get(device1);
                    Ports[] p1 = tempInventory.get(device1).getPorts();
                    Ports[] p2 = null, p3 = null;
                    //String ipToDelete1 = null, ipToDelete2 = null, ipToDelete3 = null;
                    //Devices td = null, d3 = null;
                    for (int i = 0; i < p1.length; i++){
                        if (p1[i].getConnected()){
                            d2 = tempInventory.get(p1[i].getDestination());
                            p2 = d2.getPorts();
                            for (int j = 0; j < p2.length; j++){
                                if (p2[j].getConnected()){
                                    //System.out.println(p1[i].getDestination() +"-"+ p2[j].getSource() 
                                           // + "-"+ p2[j].getDestination() +"-"+ p1[i].getSource());
                                    if(p1[i].getDestination() == p2[j].getSource() 
                                            && p2[j].getDestination() == p1[i].getSource()){
                                        p2[j].setConnected(false);
                                        p2[j].setDestination(-6);
                                        p2[j].setSource(-6);
                                    }
                                } /*else {
                                     System.out.println("This is a trap");
                                }*/
                            }
                            //System.out.println("***");
                            
                        } /*else{
                            System.out.println("This is a trap");
                        }*/
                    }
                    tempInventory.remove(device1);
                    System.out.println("Dispositivo eliminado\n\n");
            }else{
            System.out.println("Es necesario al menos un dispositivo para utilizar esta opcion\n");
        }
    }
    
    
    public static void menuMessage(){
        String ct; //ct for critic
        System.out.println("");
        System.out.println("1. Enviar un mensaje");
        System.out.println("2. Revisar historial de mensajes");
        System.out.print("Ingrese su numero de opcion: ");
        ct = sc.nextLine();
        switch(ct){
            case "1":
                sendMessage();
                break;
            case "2":
                showMessageLog();
                break;
            default:
                System.out.println("Intrese una opcion valida");
                menuMessage();
        }
    }
    
    public static boolean moreThanTwoHosts(){
        Devices device = null;
        int hosts = 0;
        for (int i = 0; i < tempInventory.size(); i++){
            device = tempInventory.get(i);
            if (device.getType().equals("Host")){
                hosts++;
            }
        }
        if (hosts > 1){
            return true;
        } else{
            return false;
        }
    }
    
public static void sendMessage(){//This method send a message from dev1 and dev2 #PANIC MODE: ON
        ArrayList<Integer> temp = new ArrayList<Integer>();
        int device1, device2;
        String nameDev1 = "", nameDev2 = "", message;
        Devices d1 = null;//all the variables was created for save the device and the route device
        Devices d2 = null;
        Devices r1 = null;
        //Devices r2 = null;
        boolean control = false;
       if(moreThanTwoHosts()){
            System.out.println("Dispositivos disponibles: ");
            for (int i = 0; i < tempInventory.size(); i++){//show the devices
                  d1 = tempInventory.get(i);
                  Ports[] ports = d1.getPorts();
                  if(!d1.getType().equals("Router") && !d1.getType().equals("Switch")){
                         System.out.println((i) + " - " +tempInventory.get(i).getType()
                         + " " + tempInventory.get(i).getName() + ": " + 
                         tempInventory.get(i).getIP());
                  } else {
                      temp.add(i);
                  }
               }
                do {
                    device1 = inputInt("Ingresar el numero del dispositivo emisor: ", tempInventory.size());//take the device to compare
                    if (temp.contains(device1)){
                        System.out.println("El dispositivo indicado no es capaz de enviar mensajes");
                    }
                } while(temp.contains(device1));
                d1 = tempInventory.get(device1);  
                nameDev1 = d1.getName();
                String [] ip1 = d1.getIP().split("\\.");
                //r1 = tempInventory.get(d1.getPorts()[0].getDestination());
                    /*//System.out.println(d1.getPorts()[0].getSource());//identity
                    //System.out.println(d1.getPorts()[0].getDestination());//where is connected*/
                do {
                    device2 = inputInt("Ingresar el numero del dispositivo receptor: ", tempInventory.size());
                    if (temp.contains(device2)){
                        System.out.println("El dispositivo indicado no es capaz de recibir mensajes");
                    }
                    if (device1 == device2){
                        System.out.println("No es posible enviar mensajes al mismo dispositivo");
                    }
                } while(temp.contains(device2) || device1 == device2);
                d2 = tempInventory.get(device2);
                nameDev2 = d2.getName();
                String [] ip2 = d2.getIP().split("\\.");
                //r2 = tempInventory.get(d2.getPorts()[0].getDestination());
                    /*//System.out.println(d2.getPorts()[0].getSource());//identity
                    //System.out.println(d2.getPorts()[0].getDestination());//where is connected*/
                //System.out.println(nameDev1 + " & " + nameDev2);
                if(d1.getPorts()[0].getConnected() == true && d2.getPorts()[0].getConnected() == true)//same IP
                {
                    if(d1.getPorts()[0].getDestination() == d2.getPorts()[0].getSource() && 
                            d2.getPorts()[0].getDestination() == d1.getPorts()[0].getSource()){
                            System.out.println("Ingrese el mensage a compartir: ");
                            message = sc.nextLine();                                
                            messageLog.add(message + "\tDe: " + d1.getName() + " - Para: " + d2.getName());
                            System.out.println("Mensaje enviado");
                    } else{
                        r1 = tempInventory.get(d1.getPorts()[0].getDestination());
                        System.out.println("Ingrese el mensage a compartir: ");
                        message = sc.nextLine();                                
                        control = mae(r1, d1.getPorts()[0].getSource(), d2.getPorts()[0].getSource());
                        if (control){
                            messageLog.add(message + "\tDe: " + nameDev1 + " - Para: " + nameDev2);
                            System.out.println("Mensaje enviado");
                        }
                        System.out.println(control ? "Enviado" : "Fallido");
                    }
                }else  {
                        System.out.println("\nError #675\nImposible enviar el mensaje, verifique que los dispositivos"
                               + " esten conectados entre si e intente de nuevo ");
                       //mainMenu();
                        }
                    //System.out.println(test);
                    //System.out.println(nameDev1 + " & " + nameDev2);
            
            }else{
           System.out.println("Es necesario al menos dos dispositivos para utilizar esta opcion\n");
           //mainMenu();
            }    //this section add and compare the IP to send the message
      }
      
      public static void showMessageLog(){//DB for the message between the hosts
        System.out.println("Mostrando todos los mensajes compartidos entre dispositivos: ");  
        Iterator<String> show = messageLog.iterator();
        if(show.hasNext()){
            while(show.hasNext())
            System.out.println("-"+show.next());
        }else
        {
            System.out.println("No hay mensajes");
        }
      }
      
      public static boolean mae(Devices dad, int start, int end){
          //System.out.println(dad.getName());
          int destination;
          boolean control = false;
          Ports[] ports = null;
          Devices son;
          for (int i = 0;  i < dad.getNPorts(); i++){
              if (control) {
                  return true;
              } else {
                  ports = dad.getPorts();
                  destination = ports[i].getDestination();
                  if (ports[i].getConnected()){
                      if (destination != start){
                          //System.out.println(destination);
                          if (destination == end){
                              System.out.println("***");
                              return true;
                          } else{
                              son = tempInventory.get(destination);
                              if (!son.getType().equals("Host")){
                                  start = ports[i].getSource();
                                  control = mae(son, start, end);
                              } else{
                                  control = false;
                              }
                          }
                      } else {              
                          control = false;
                      }
                  } else {
                      control = false;
                  } 
              }
          }
          return control;
      }
}
