/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package networking;

/**
 *
 * @author fx62 & Luxo Lederhand
 */
public class Devices {
    
    
    //private static ArrayList<Integer> temp = new ArrayList<Integer>();
    private String type;
    private String name;
    //private ArrayList<String> arp;
    private String ip;
    private boolean active;
    private Ports[] ports;
    private int nports;
    private String ip2;
    
    public Devices(String type, String name, String ip, 
            Ports[] ports, int nports) {
        this.type = type;
        this.name =name;
        //this.arp = arp;
        this.ip = ip;
        this.active = false;
        this.ports = ports;
        this.nports = nports;
        this.ip2 = ip2;
    }
    
    public Devices() {
        this.type = "unknow";
        this.name = "unknow";
        //this.arp = null;
        this.ip = "169.254.0.0/16";
        this.active = false;
    }
    
    public String getType(){
        return type;
    }
    
    public String getName(){
        return name;
    }
    
    public String getIP(){
        return ip;
    }
    
    public Ports[] getPorts(){
        return ports;
    }
    
    public int getNPorts(){
        return nports;
    }
    
    public String getIP2(){
        return ip2;
    }
    
    public void SetName(String name){
        this.name = name;
    }
    
    public void SetIP(String ip){
        this.ip = ip;
    }
    
    public void SetPorts(Ports[] ports){
        this.ports = ports;
    }
    
    public void SetIP2(String ip2){
        this.ip2 = ip2;
    }
    
    // There is not possible to modify the number of ports
    
    public boolean getActive(){
        return active;
    }
    
    public void setActive(boolean active){
        this.active = active;
    }
    
    public void getActive(boolean active){
        this.active = active;
    }
    
    public void instancePorts(Ports[] ports, int nports){
        for (int i = 0; i < nports; i++){
            ports[i] = new Ports(-6, -6);
        }
    }
    
    public boolean sendMessage(){
        return true;
    }
}

